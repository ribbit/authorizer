package:
	sam build
	sam package --s3-bucket io.andrewohara.pipeline --output-template-file .aws-sam/out.yml

deploy: package
	aws cloudformation deploy --template-file .aws-sam/out.yml --stack-name ribbit-authorizer-dev --capabilities CAPABILITY_IAM