from os import environ

from pyauthlib import UserInfo, AuthPolicy, HttpMethod, parse_event, raise_401
from .google import GoogleTokenValidator

VALIDATOR = GoogleTokenValidator(google_client_id=environ["GOOGLE_CLIENT_ID"])


def handle_auth(event, _context):
    event = parse_event(event)

    user_id: str = VALIDATOR.get_user_id(event.access_token)
    if user_id is None:
        raise_401()

    user_info = UserInfo(principal_id=user_id, authorities=[])

    policy = AuthPolicy(user_info)
    policy.allow(event.arn(method=HttpMethod.ALL, resource="*"))
    return policy.build()
