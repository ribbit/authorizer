from typing import Optional

from google.oauth2.id_token import verify_oauth2_token
from google.auth.transport import requests

ALLOWED_ISSUERS = [
    "accounts.google.com",
    "https://accounts.google.com",
]


class GoogleTokenValidator(object):
    def __init__(self, google_client_id: str):
        self.google_client_id = google_client_id

    def get_user_id(self, id_token: str) -> Optional[str]:
        request = requests.Request()
        try:
            user_info = verify_oauth2_token(id_token, request, self.google_client_id)
        except ValueError:
            return None  # invalid token
        if user_info["iss"] not in ALLOWED_ISSUERS:
            print("Wrong issuer: " + user_info["iss"])
            return None
        return user_info["sub"]
